json.extract! schedule, :id, :schedule_name, :schedule_year, :created_at, :updated_at
json.url schedule_url(schedule, format: :json)
