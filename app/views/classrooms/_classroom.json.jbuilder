json.extract! classroom, :id, :class_room_nbre, :class_room_name, :created_at, :updated_at
json.url classroom_url(classroom, format: :json)
