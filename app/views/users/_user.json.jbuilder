json.extract! user, :id, :first_name, :last_name, :cin, :date_of_birth, :email, :pwd, :tel_nbr, :address, :created_at, :updated_at
json.url user_url(user, format: :json)
