json.extract! module_subject, :id, :module_name, :module_coefficient, :created_at, :updated_at
json.url module_subject_url(module_subject, format: :json)
