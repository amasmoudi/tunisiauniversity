json.extract! seance, :id, :start_time, :end_time, :actual_date, :created_at, :updated_at
json.url seance_url(seance, format: :json)
