json.extract! professor, :id, :professor_grade, :user_id, :created_at, :updated_at
json.url professor_url(professor, format: :json)
