json.extract! student, :id, :student_special_info, :user_id, :created_at, :updated_at
json.url student_url(student, format: :json)
