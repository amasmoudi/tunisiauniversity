json.extract! class, :id, :class_name, :class_capacity, :class_specific_id, :created_at, :updated_at
json.url class_url(class, format: :json)
