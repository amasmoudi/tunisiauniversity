class Professor < ActiveRecord::Base
  belongs_to :user
  has_many :presences
end
