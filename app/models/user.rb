class User < ActiveRecord::Base
	has_many :professors
	has_many :students
	attr_accessor :professor_grade , :user_id , :student_special_info
end
