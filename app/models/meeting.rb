class Meeting < ActiveRecord::Base
	has_one :classroom
	has_one :subject
	belongs_to :schedule
end
