class Seance < ActiveRecord::Base
	has_one :subject
	has_one :class
	belongs_to :schedule
end
