class ModuleSubjectsController < ApplicationController
  before_action :set_module_subject, only: [:show, :edit, :update, :destroy]

  # GET /module_subjects
  # GET /module_subjects.json
  def index
    @module_subjects = ModuleSubject.all
  end

  # GET /module_subjects/1
  # GET /module_subjects/1.json
  def show
  end

  # GET /module_subjects/new
  def new
    @module_subject = ModuleSubject.new
  end

  # GET /module_subjects/1/edit
  def edit
  end

  # POST /module_subjects
  # POST /module_subjects.json
  def create
    @module_subject = ModuleSubject.new(module_subject_params)

    respond_to do |format|
      if @module_subject.save
        format.html { redirect_to @module_subject, notice: 'Module subject was successfully created.' }
        format.json { render :show, status: :created, location: @module_subject }
      else
        format.html { render :new }
        format.json { render json: @module_subject.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /module_subjects/1
  # PATCH/PUT /module_subjects/1.json
  def update
    respond_to do |format|
      if @module_subject.update(module_subject_params)
        format.html { redirect_to @module_subject, notice: 'Module subject was successfully updated.' }
        format.json { render :show, status: :ok, location: @module_subject }
      else
        format.html { render :edit }
        format.json { render json: @module_subject.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /module_subjects/1
  # DELETE /module_subjects/1.json
  def destroy
    @module_subject.destroy
    respond_to do |format|
      format.html { redirect_to module_subjects_url, notice: 'Module subject was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_module_subject
      @module_subject = ModuleSubject.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def module_subject_params
      params.require(:module_subject).permit(:module_name, :module_coefficient)
    end
end
