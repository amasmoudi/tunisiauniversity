require 'test_helper'

class ModuleSubjectsControllerTest < ActionController::TestCase
  setup do
    @module_subject = module_subjects(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:module_subjects)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create module_subject" do
    assert_difference('ModuleSubject.count') do
      post :create, module_subject: { module_coefficient: @module_subject.module_coefficient, module_name: @module_subject.module_name }
    end

    assert_redirected_to module_subject_path(assigns(:module_subject))
  end

  test "should show module_subject" do
    get :show, id: @module_subject
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @module_subject
    assert_response :success
  end

  test "should update module_subject" do
    patch :update, id: @module_subject, module_subject: { module_coefficient: @module_subject.module_coefficient, module_name: @module_subject.module_name }
    assert_redirected_to module_subject_path(assigns(:module_subject))
  end

  test "should destroy module_subject" do
    assert_difference('ModuleSubject.count', -1) do
      delete :destroy, id: @module_subject
    end

    assert_redirected_to module_subjects_path
  end
end
