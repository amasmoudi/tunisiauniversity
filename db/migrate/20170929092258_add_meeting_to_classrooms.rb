class AddMeetingToClassrooms < ActiveRecord::Migration
  def change
    add_reference :classrooms, :meeting, index: true, foreign_key: true
  end
end
