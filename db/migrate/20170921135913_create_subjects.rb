class CreateSubjects < ActiveRecord::Migration
  def change
    create_table :subjects do |t|
      t.string :subject_name
      t.integer :subject_year
      t.string :subject_hour
      t.float :subject_coefficient
      t.references :module_subject, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
