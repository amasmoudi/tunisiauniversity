class CreateSchedules < ActiveRecord::Migration
  def change
    create_table :schedules do |t|
      t.string :schedule_name
      t.datetime :schedule_year

      t.timestamps null: false
    end
  end
end
