class CreatePresences < ActiveRecord::Migration
  def change
    create_table :presences do |t|
      t.datetime :in_class
      t.datetime :out_class
      t.string :boitier

      t.timestamps null: false
    end
  end
end
