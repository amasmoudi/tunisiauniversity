class CreateMeetings < ActiveRecord::Migration
  def change
    create_table :meetings do |t|
      t.datetime :start_time
      t.datetime :end_time
      t.datetime :actual_date

      t.timestamps null: false
    end
  end
end
