class CreateProfessors < ActiveRecord::Migration
  def change
    create_table :professors do |t|
      t.string :professor_grade
      t.references :user, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
