class AddScheduleToSeances < ActiveRecord::Migration
  def change
    add_reference :seances, :schedule, index: true, foreign_key: true
  end
end
