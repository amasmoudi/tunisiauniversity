class AddStudentToPresences < ActiveRecord::Migration
  def change
    add_reference :presences, :student, index: true, foreign_key: true
  end
end
