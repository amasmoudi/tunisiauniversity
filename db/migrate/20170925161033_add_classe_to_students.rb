class AddClasseToStudents < ActiveRecord::Migration
  def change
    add_reference :students, :classe, index: true
  end
end
