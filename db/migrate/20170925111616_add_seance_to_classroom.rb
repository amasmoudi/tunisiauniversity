class AddSeanceToClassroom < ActiveRecord::Migration
  def change
    add_reference :classrooms, :seance, index: true, foreign_key: true
  end
end
