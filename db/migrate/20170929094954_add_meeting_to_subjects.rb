class AddMeetingToSubjects < ActiveRecord::Migration
  def change
    add_reference :subjects, :meeting, index: true, foreign_key: true
  end
end
