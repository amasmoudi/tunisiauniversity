class CreateClasses < ActiveRecord::Migration
  def change
    create_table :classes do |t|
      t.string :class_name
      t.integer :class_capacity
      t.string :class_specific_id

      t.timestamps null: false
    end
  end
end
