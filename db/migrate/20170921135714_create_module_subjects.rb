class CreateModuleSubjects < ActiveRecord::Migration
  def change
    create_table :module_subjects do |t|
      t.string :module_name
      t.float :module_coefficient

      t.timestamps null: false
    end
  end
end
