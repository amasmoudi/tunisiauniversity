class CreateStudents < ActiveRecord::Migration
  def change
    create_table :students do |t|
      t.string :student_special_info
      t.references :user, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
