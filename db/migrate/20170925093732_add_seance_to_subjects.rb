class AddSeanceToSubjects < ActiveRecord::Migration
  def change
    add_reference :subjects, :seance, index: true, foreign_key: true
  end
end
