class AddScheduleToMeetings < ActiveRecord::Migration
  def change
    add_reference :meetings, :schedule, index: true, foreign_key: true
  end
end
