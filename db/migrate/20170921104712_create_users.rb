class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :first_name
      t.string :last_name
      t.string :cin
      t.datetime :date_of_birth
      t.string :email
      t.string :pwd
      t.string :tel_nbr
      t.text :address

      t.timestamps null: false
    end
  end
end
