class AddProfessorToPresences < ActiveRecord::Migration
  def change
    add_reference :presences, :professor, index: true, foreign_key: true
  end
end
