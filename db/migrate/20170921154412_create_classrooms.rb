class CreateClassrooms < ActiveRecord::Migration
  def change
    create_table :classrooms do |t|
      t.string :class_room_nbre
      t.string :class_room_name

      t.timestamps null: false
    end
  end
end
