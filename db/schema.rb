# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170929100503) do

  create_table "classes", force: :cascade do |t|
    t.string   "class_name"
    t.integer  "class_capacity"
    t.string   "class_specific_id"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
  end

  create_table "classrooms", force: :cascade do |t|
    t.string   "class_room_nbre"
    t.string   "class_room_name"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
    t.integer  "seance_id"
    t.integer  "meeting_id"
  end

  add_index "classrooms", ["meeting_id"], name: "index_classrooms_on_meeting_id"
  add_index "classrooms", ["seance_id"], name: "index_classrooms_on_seance_id"

  create_table "meetings", force: :cascade do |t|
    t.datetime "start_time"
    t.datetime "end_time"
    t.datetime "actual_date"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.integer  "schedule_id"
  end

  add_index "meetings", ["schedule_id"], name: "index_meetings_on_schedule_id"

  create_table "module_subjects", force: :cascade do |t|
    t.string   "module_name"
    t.float    "module_coefficient"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
  end

  create_table "presences", force: :cascade do |t|
    t.datetime "in_class"
    t.datetime "out_class"
    t.string   "boitier"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
    t.integer  "professor_id"
    t.integer  "student_id"
  end

  add_index "presences", ["professor_id"], name: "index_presences_on_professor_id"
  add_index "presences", ["student_id"], name: "index_presences_on_student_id"

  create_table "professors", force: :cascade do |t|
    t.string   "professor_grade"
    t.integer  "user_id"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
  end

  add_index "professors", ["user_id"], name: "index_professors_on_user_id"

  create_table "schedules", force: :cascade do |t|
    t.string   "schedule_name"
    t.datetime "schedule_year"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
    t.integer  "classe_id"
  end

  add_index "schedules", ["classe_id"], name: "index_schedules_on_classe_id"

  create_table "seances", force: :cascade do |t|
    t.datetime "start_time"
    t.datetime "end_time"
    t.datetime "actual_date"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.integer  "schedule_id"
  end

  add_index "seances", ["schedule_id"], name: "index_seances_on_schedule_id"

  create_table "students", force: :cascade do |t|
    t.string   "student_special_info"
    t.integer  "user_id"
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
    t.integer  "classe_id"
  end

  add_index "students", ["classe_id"], name: "index_students_on_classe_id"
  add_index "students", ["user_id"], name: "index_students_on_user_id"

  create_table "subjects", force: :cascade do |t|
    t.string   "subject_name"
    t.integer  "subject_year"
    t.string   "subject_hour"
    t.float    "subject_coefficient"
    t.integer  "module_subject_id"
    t.datetime "created_at",          null: false
    t.datetime "updated_at",          null: false
    t.integer  "seance_id"
    t.integer  "meeting_id"
  end

  add_index "subjects", ["meeting_id"], name: "index_subjects_on_meeting_id"
  add_index "subjects", ["module_subject_id"], name: "index_subjects_on_module_subject_id"
  add_index "subjects", ["seance_id"], name: "index_subjects_on_seance_id"

  create_table "users", force: :cascade do |t|
    t.string   "first_name"
    t.string   "last_name"
    t.string   "cin"
    t.datetime "date_of_birth"
    t.string   "email"
    t.string   "pwd"
    t.string   "tel_nbr"
    t.text     "address"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
    t.string   "user_type"
  end

end
